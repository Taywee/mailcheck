//use std::ffi::{CStr};
use std::os::raw::{c_char, c_int};
use std::path::Path;
use std::io;
use std::str;
use std::slice;

#[repr(C)]
pub struct lua_State {
    private: [u8; 0],
}

#[allow(non_camel_case_types)]
type lua_CFunction = extern "C" fn(*mut lua_State) -> c_int;
#[allow(non_camel_case_types)]
type lua_Number = f64;

#[link(name = "lua")]
extern "C" {
    //fn luaL_newstate() -> *mut lua_State;
    fn lua_close(L: *mut lua_State);
    fn lua_createtable(L: *mut lua_State, narr: c_int, nrec: c_int);
    //fn lua_setfield(L: *mut lua_State, index: c_int, key: *const c_char);
    fn lua_settable(L: *mut lua_State, index: c_int);
    fn lua_pushnil(L: *mut lua_State);
    fn lua_pushcclosure(L: *mut lua_State, function: lua_CFunction, n: c_int);
    fn lua_pushlstring(L: *mut lua_State, s: *const c_char, len: usize);
    fn lua_pushnumber(L: *mut lua_State, n: lua_Number);
    fn lua_tolstring(L: *mut lua_State, index: c_int, len: *mut usize) -> *const c_char;
    fn lua_gettop(L: *mut lua_State) -> c_int;
}

struct Lua {
    state: *mut lua_State,
    cleanup: bool,
}

impl Lua {
    /*pub fn newstate() -> Lua {
        Lua {
            state: unsafe { luaL_newstate() },
            cleanup: true,
        }
    }*/

    /*/// Take ownership of the state
    pub fn take_state(state: *mut lua_State) -> Lua {
        Lua {
            state,
            cleanup: true,
        }
    }*/

    /// Borrow the state, don't clean it up.
    pub fn borrow_state(state: *mut lua_State) -> Lua {
        Lua {
            state,
            cleanup: false,
        }
    }

    pub fn createtable(&mut self, narr: c_int, nrec: c_int) {
        unsafe {
            lua_createtable(self.state, narr, nrec);
        }
    }

    /*pub fn setfield<S>(&mut self, narr: c_int, key: S)
    where
        S: AsRef<CStr>,
    {
        unsafe {
            lua_setfield(self.state, narr, key.as_ref().as_ptr());
        }
    }*/

    pub fn settable(&mut self, narr: c_int)
    {
        unsafe {
            lua_settable(self.state, narr);
        }
    }

    pub fn pushcclosure(&mut self, function: lua_CFunction, n: c_int) {
        unsafe {
            lua_pushcclosure(self.state, function, n);
        }
    }

    pub fn push_nil(&mut self)
    {
        unsafe {
            lua_pushnil(self.state);
        }
    }

    pub fn push_number(&mut self, n: lua_Number)
    {
        unsafe {
            lua_pushnumber(self.state, n);
        }
    }

    pub fn push_str<S>(&mut self, s: S)
    where
        S: AsRef<str>,
    {
        unsafe {
            let s = s.as_ref();
            lua_pushlstring(self.state, s.as_ptr() as *const c_char, s.len());
        }
    }

    pub fn to_str(&mut self, index: c_int) -> Result<&str, String> {
        let top = unsafe { lua_gettop(self.state) };
        let index = if index < 0 {
            top + index + 1
        } else {
            index
        };
        if index > 0 && index <= top {
            let mut len: usize = 0;
            unsafe {
                let ptr = lua_tolstring(self.state, index, &mut len as *mut usize) as *const u8;
                let slice: &[u8] = slice::from_raw_parts(ptr, len);
                str::from_utf8(slice).map_err(|e| e.to_string())
            }
        } else {
            Err("Invalid index".into())
        }
    }
}

impl Drop for Lua {
    fn drop(&mut self) {
        if self.cleanup {
            unsafe {
                lua_close(self.state);
            }
        }
    }
}

fn scan<P>(path: P) -> io::Result<Vec<(String, usize)>>
where P: AsRef<Path>
{
    let mut output = Vec::new();
    for direntry in path.as_ref().read_dir()? {
        let direntry = direntry?;
        let mailbox = direntry.file_name();
        let mut path = direntry.path();
        path.push("INBOX/new");
        if path.is_dir() {
            let count = path.read_dir()?.count();
            if let Ok(s) = mailbox.into_string() {
                output.push((s, count));
            }
        }
    }
    output.sort_unstable_by(|a, b| a.cmp(b));
    Ok(output)
}

/// Scan the mail directory given as an argument and return a sorted table of mail directories and
/// counts.  Each mail directory will be a table of "name" and "count", so the full structure will
/// be something like this:
/// 
///     {
///         {
///             name = "foo@example.com",
///             count = 15,
///         },
///         {
///             name = "bar@example.com",
///             count = 0,
///         }
///     }
///
/// Any errors will be returned as two values: a nil and an error message, so be sure to check.
pub extern "C" fn scan_maildir(l: *mut lua_State) -> c_int {
    let mut state = Lua::borrow_state(l);
    match state.to_str(-1) {
        Ok(s) => {
            match scan(s) {
                Ok(v) => {
                    state.createtable(v.len() as i32, 0);
                    for (i, (name, count)) in v.iter().enumerate() {
                        // Proper lua table form
                        state.push_number((i + 1) as lua_Number);
                        state.createtable(0, 2);
                        state.push_str("name");
                        state.push_str(name);
                        state.settable(-3);
                        state.push_str("count");
                        state.push_number(*count as lua_Number);
                        state.settable(-3);
                        // Actually set the table as the index given
                        state.settable(-3);
                    }
                    1
                }
                Err(e) => {
                    // Return an error
                    state.push_nil();
                    state.push_str(e.to_string());
                    2
                }
            }
        },
        Err(e) => {
            // Return an error
            state.push_nil();
            state.push_str(e);
            2
        }
    }
}

#[no_mangle]
pub extern "C" fn luaopen_libmailcheck(l: *mut lua_State) -> c_int {
    let mut state = Lua::borrow_state(l);
    state.createtable(0, 0);
    state.push_str("scan");
    state.pushcclosure(scan_maildir, 0);
    state.settable(-3);
    1
}
